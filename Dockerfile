ARG PY_TAG="alpine"
ARG DISTR_OUT="scratch"

FROM python:${PY_TAG} as build

ARG ANSIBLE_VERSION="2.17"

ENV ROOT_ETC="etc"
ENV ROOT_BIN="bin"
ENV ROOT_LIB="lib"

ENV USR_BIN="usr/bin"
ENV USR_LIB="usr/lib"
ENV USR_SBIN="usr/sbin"
ENV USR_SHARE="usr/share"

ENV PY_BIN="usr/local/bin"
ENV PY_LIB="usr/local/lib"

ENV ANS_PWD="home/ans"
ENV LANG C.UTF-8

RUN apk add --update-cache --no-cache ca-certificates openssh git gcc musl-dev libffi-dev

RUN mkdir -p /tmp/root/${ROOT_BIN} /tmp/root/${ROOT_LIB} /tmp/root/${USR_BIN} /tmp/root/${USR_LIB} /tmp/root/${PY_BIN} /tmp/root/${PY_LIB} 
#     
# ansible 
RUN if ["${ANSIBLE_VERSION}" == "DEV"]; then \
      git clone https://github.com/ansible/ansible.git &&\
      cd ansible &&\
      source ./hacking/env-setup &&\
      python3 -m pip install -r ./requirements.txt &&\
      mkdir -p /tmp/root/ansible/test &&\
      cp -ar /ansible/bin/ /ansible/lib/  /tmp/root/ansible/ &&\
      cp -ar /ansible/test/lib/ /tmp/root/ansible/test/; \
    else \
      python3 -m pip install --no-cache-dir ansible-core==${ANSIBLE_VERSION} &&\
      cp -a /${PY_BIN}/ansible* /tmp/root/${PY_BIN}; \
    fi &&\
    python3 -m pip install molecule molecule-docker molecule-containers &&\
    # ansible dependency
    cp -a /${USR_BIN}/env /${USR_BIN}/sftp /${USR_BIN}/scp /tmp/root/${USR_BIN}/ &&\
    cp -a /${USR_LIB}/libffi.* /tmp/root/${USR_LIB}/

# python3
RUN cp -a /${PY_BIN}/python* /${PY_BIN}/pip* /tmp/root/${PY_BIN} &&\
    cp -ar /${PY_LIB}/*python3.* /tmp/root/${PY_LIB} &&\
    # dependency pip
    cp -a /${USR_LIB}/libexpat.* /tmp/root/${USR_LIB}
#
# ssh
RUN mkdir -p /tmp/root/${ROOT_ETC}/ssh &&\
    cp -a /${USR_BIN}/ssh*  /tmp/root/${USR_BIN} &&\
    cp -a /${ROOT_ETC}/passwd /tmp/root/${ROOT_ETC}
# RUN cp -ar /${ROOT_ETC}/ssh/ /tmp/root/${ROOT_ETC}
# git
RUN cp -a /${USR_BIN}/git* /tmp/root/${USR_BIN} &&\
    mkdir -p /tmp/root/${USR_LIB}exec/git-core &&\
    cp -a /${USR_LIB}exec/git-core/git-remote-http* /tmp/root/${USR_LIB}exec/git-core
RUN mkdir -p /tmp/root/${USR_SHARE}/ &&\
    cp -ar /${USR_SHARE}/git-core/ /tmp/root/${USR_SHARE}/
# 
# dependency
RUN cp -a /${ROOT_LIB}/libc.musl-* /${ROOT_LIB}/ld-musl-* /${ROOT_LIB}/libcrypto.* /${ROOT_LIB}/libz.* /tmp/root/${ROOT_LIB}
# 
# dependency git
RUN cp -a /${USR_LIB}/libcurl.* /${USR_LIB}/libcares.* /${USR_LIB}/libnghttp2.* /${USR_LIB}/libidn2.* /${USR_LIB}/libpsl.* /${USR_LIB}/libzstd.* /${USR_LIB}/libbrotlidec.* /${USR_LIB}/libunistring.* /${USR_LIB}/libbrotlicommon.* /tmp/root/${USR_LIB} &&\      
    cp -a /${ROOT_LIB}/libssl.* /tmp/root/${ROOT_LIB}
RUN mkdir -p /tmp/root/${ROOT_ETC}/ssl/ &&\
    cp -a /${ROOT_ETC}/ssl/certs/ /tmp/root/${ROOT_ETC}/ssl/
# 
# ash dependency
RUN cp -a /${USR_LIB}/libpcre2-* /tmp/root/${USR_LIB}
# RUN cd /home/ans/bin/ && cp -a ansible* /tmp/root/usr/bin/ 
# # 
# sheel
RUN cp -a /${ROOT_BIN}/base64 /${ROOT_BIN}/busybox /${ROOT_BIN}/cat /${ROOT_BIN}/chmod /${ROOT_BIN}/echo /${ROOT_BIN}/hostname /${ROOT_BIN}/ls /${ROOT_BIN}/printenv /${ROOT_BIN}/mkdir /${ROOT_BIN}/sh /${ROOT_BIN}/grep /${ROOT_BIN}/touch /tmp/root/${ROOT_BIN}/

RUN mkdir -p /tmp/root/${USR_SBIN} /${USR_SHARE}/ca-certificates &&\
    cp -a /${USR_SBIN}/update-ca-certificates /tmp/root/${USR_SBIN} &&\
    cp -ar /${ROOT_ETC}/ca-certificates* /tmp/root/${ROOT_ETC}/ &&\
    cp -ar /${USR_SHARE}/ca-certificates* /tmp/root/${USR_SHARE}/
# 
# FROM alpine:latest as runtime
FROM ${DISTR_OUT}

ENV LANG C.UTF-8
ENV PATH /ansible/bin:/bin:/usr/bin:/usr/local/bin:$PATH
ENV PYTHONPATH /ansible/lib:/ansible/test/lib:/usr/local/lib/python3.12/site-packages
ENV SSL_CERT_DIR=/etc/ssl/certs

WORKDIR /home/

COPY --from=build /tmp/* /
# COPY . .

LABEL "License"="MIT" "author"="Подрезенко Виталий @vitoxaya"

ENTRYPOINT ["ansible", "--version"]
