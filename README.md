# Dockerfile Ansible + molecule
---
CI для создания Docker образа Ansible + molecule
-------------
---
Docker Build Arg
--------------
      # Тег Dоcker образа python для stage build
      PY_TAG="alpine" # 3.8-alpine
      # Docker образ конечного образа на выходе
      DISTR_OUT="scratch"
      # Версия Ansible, если указать DEV то будет установлен ansible developer version из github
      ANSIBLE_VERSION="2.17" # "DEV"
---      
Example Build command
----------------
      docker build -t ansible:scratch --build-arg ANSIBLE_VERSION="2.17" .
---
License
-------

MIT

Author Information
------------------

Подрезенко Виталий
@vitoxaya
